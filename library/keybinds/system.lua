local awful = require('awful')

local apps	= require("settings.default_apps")
local paths	= require("settings.paths")

local group_name = "System management"


-- SYSTEM
awful.keyboard.append_global_keybindings {
	-- Notifications
	-- ctrl + space hide last notification
	-- dunst like commands..

	-- Volume control
	awful.key({}, "XF86AudioRaiseVolume",
		function() awesome.emit_signal("system::volume:up") end,
		{group = group_name, description= "Increase volume by 5%"}
	),
	awful.key({}, "XF86AudioLowerVolume",
		function() awesome.emit_signal("system::volume:down") end,
		{group = group_name, description= "Decrease volume by 5%"}
	),
	awful.key({}, "XF86AudioMute",
		function() awesome.emit_signal("system::volume:toggle") end,
		{group = group_name, description= "Toggle Mute audio"}
	),

	-- Microphone
	awful.key({}, "XF86AudioMicMute",
		function() awesome.emit_signal("system::microphone:toggle") end,
		{group = group_name, description= "Toggle mute microphone"}
	),

	-- Media control
	awful.key({}, "XF86AudioPlay",
		function() awful.spawn("playerctl play-pause") end,
		{group = group_name, description= "Toggle media player"}
	),
	awful.key({}, "XF86AudioNext",
		function() awful.spawn("playerctl next") end,
		{group = group_name, description= "Next song"}
	),
	awful.key({}, "XF86AudioPrev",
		function() awful.spawn("playerctl previous") end,
		{group = group_name, description= "Previous song"}
	),

	-- Brightness control
	awful.key({}, "XF86MonBrightnessUp",
		function() awesome.emit_signal("system::brightness:up") end,
		{group = group_name, description= "Increase brightness by 5%"}
	),
	awful.key({}, "XF86MonBrightnessDown",
		function() awesome.emit_signal("system::brightness:down") end,
		{group = group_name, description= "Decrease brightness by 5%"}
	),

	-- Session management
	-- ..

	-- Log files
	awful.key({A}, "l",
		function() awful.spawn({apps.editor_cmd, paths.log_session}) end,
		{group = group_name, description = "open log file"}
	),

	-- System monitor
	awful.key({C}, "Escape",
		function() awful.spawn(apps.terminal_alt .. " -e htop") end,
		{group = group_name, description= "Spawn system monitor cli"}
	)
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
