local awful = require('awful')
local helpers	= require("library.helpers.keybinds")

local group_name = "Tags"


-- TODO: Change view with arrow key
-- TAGS
awful.keyboard.append_global_keybindings {
	awful.key({M}, "Left",
		awful.tag.viewprev,
		{group = group_name, description = "view previous"}
	),
	awful.key({M}, "Right",
		awful.tag.viewnext,
		{group = group_name, description = "view next"}
	),
	awful.key {
		modifiers	= { M },
		key		= "Escape",
		group		= group_name,
		description	= "go back",
		on_press	= awful.tag.history.restore
	},
	-- awful.key {	-- Change view with arrow key
	--         modifiers	= { M },
	--         keygroup	= "arrows",
	--         group		= group_name,
	--         description	= "View next/prev tag",
	--         on_press	= function()
	-- },
	awful.key {	-- Change view to
		modifiers   = { M },
		keygroup    = "numrow",
		group       = group_name,
		description = "only view tag",
		on_press    = helpers.tag_view
	},
	awful.key {	-- Move window to
		modifiers = { M, S },
		keygroup    = "numrow",
		group       = group_name,
		description = "move focused client to tag",
		on_press    = helpers.tag_move_client
	},
	awful.key {	-- XXX: ??
		modifiers   = { M, C },
		keygroup    = "numrow",
		group       = group_name,
		description = "toggle tag",
		on_press    = helpers.tag_toggle
	},
	awful.key {	-- XXX: ??
		modifiers   = { M, C, S },
		keygroup    = "numrow",
		group       = group_name,
		description = "toggle focused client on tag",
		on_press    = helpers.tag_toggle_client
	}
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
