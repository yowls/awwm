----
-- MOUSEBINDS
----

local awful = require('awful')
local menu = require("library.menu")


-- Global Desktop mousebinds
awful.mouse.append_global_mousebindings {
	awful.button({}, 3,	function () menu.main:toggle() end),
	awful.button({}, 4,	awful.tag.viewnext),
	awful.button({}, 5,	awful.tag.viewprev)
}


-- Clients movements
client.connect_signal("request::default_mousebindings", function()
	awful.mouse.append_client_mousebindings {
		awful.button({ }, 1,
			function(c) c:activate	{context = "mouse_click"} end
		),
		awful.button({M}, 1,
			function(c) c:activate	{context = "mouse_click", action = "mouse_move"  } end
		),
		awful.button({A}, 1,
			function(c) c:activate	{context = "mouse_click", action = "mouse_resize"} end
		),
		awful.button({M}, 3,
			function(c) c:activate	{context = "mouse_click", action = "mouse_resize"} end
		)
	}
end)
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
