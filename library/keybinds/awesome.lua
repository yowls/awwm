local awful = require('awful')
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- require("awful.hotkeys_popup.keys")	-- extend hotkeys popup

local paths	= require("settings.paths")

local group_name = {
	wm		= "Awesome",
	prompt	= "Launcher"
}

-- Set menubar prompt setting
menubar.utils.terminal = require("settings.default_apps").terminal


-- BASIC WM CONTROL
awful.keyboard.append_global_keybindings {
	awful.key({M, C}, "q",
		awesome.quit,
		{group = group_name.wm, description = "quit awesome"}
	),
	awful.key({M, C}, "r",
		awesome.restart,
		{group = group_name.wm, description = "reload awesome"}
	),
	awful.key({M, S}, "s",
		hotkeys_popup.show_help,
		{group = group_name.wm, description = "show help"}
	),

	-- Prompts
	-- FIXME: fix prompt widgets (both)
	awful.key({A}, "F2",
		function() menubar.show() end,
		{group = group_name.prompt, description = "show the menubar"}
	),
	awful.key({M}, "o",
		function () awful.screen.focused().prompt:run() end,
		{group = group_name.prompt, description = "run prompt"}
	),
	awful.key({M}, "p",
		function ()
			awful.prompt.run {
				prompt		= "Run Lua: ",
				textbox		= awful.screen.focused().prompt.widget,
				exe_callback	= awful.util.eval,
				history_path	= paths.awesome_cache .. "/history_eval"
			}
		end, {group = group_name.prompt, description = "lua execute prompt"}
	)
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
