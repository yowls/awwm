----
-- SET AWESOME KEYBINDS
----

-- Set special keys
M	= "Mod4"	-- Use the SUPER KEY as the main modifier
A	= "Mod1"	-- Use the ALT KEY as second modifier
S	= "Shift"
C	= "Control"


-- TODO: add more keybinds like in my qtile conf
-- IMPORT MODULES
require('library.keybinds.awesome')
require('library.keybinds.focus')
require('library.keybinds.layout')
require('library.keybinds.move')
require('library.keybinds.resize')
require('library.keybinds.screen')
require('library.keybinds.tag')
require('library.keybinds.client')
require('library.keybinds.mouse')
require('library.keybinds.system')
require('library.keybinds.applications')
