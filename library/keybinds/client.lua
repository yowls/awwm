local awful = require('awful')

local group_name = "Window"


-- CLIENTS
client.connect_signal("request::default_keybindings", function()
	awful.keyboard.append_client_keybindings {
		awful.key({M}, "q",
			function (c) c:kill() end,
			{group = group_name, description = "close"}
		),
		awful.key({M}, "t",
			function (c) c.ontop = not c.ontop end,
			{group = group_name, description = "toggle keep on top"}
		),
		awful.key({M}, "n",
			function (c) c.minimized = true end,
			{group = group_name, description = "minimize"}
		),
		awful.key({M}, "m",
			function (c)
				c.maximized = not c.maximized
				c:raise()
			end,
			{group = group_name, description = "Toggle maximize window"}
		),
		awful.key({M, C}, "m",
			function (c)
				c.maximized_vertical = not c.maximized_vertical
				c:raise()
			end,
			{group = group_name, description = "Toggle maximize vertically"}
		),
		awful.key({M, S}, "m",
			function (c)
				c.maximized_horizontal = not c.maximized_horizontal
				c:raise()
			end,
			{group = group_name, description = "Toggle maximize horizontally"}
		),
		awful.key({M}, "f",
			function (c)
				c.fullscreen = not c.fullscreen
				c:raise()
			end,
			{group = group_name, description = "toggle fullscreen"}
		),
		awful.key({M, C}, "space",
			awful.client.floating.toggle,
			{group = group_name, description = "toggle floating"}
		)
	}
end)
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
