local awful = require('awful')

local group_name = "Move client"


-- MOVING CLIENTS
awful.keyboard.append_global_keybindings {
	awful.key({M, S}, "j",
		function () awful.client.swap.byidx(  1) end,
		{group = group_name, description = "swap with next client by index"}
	),
	awful.key({M, S}, "k",
		function () awful.client.swap.byidx( -1) end,
		{group = group_name, description = "swap with previous client by index"}
	),
	awful.key({M, C}, "Return",
		function (c) c:swap(awful.client.getmaster()) end,
		{group = group_name, description = "move to master"}
	)
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
