local ruled = require('ruled')
local awful = require('awful')
local naughty = require('naughty')
local beautiful = require('beautiful')

local settings = require('settings.global')

-- TODO: see buttons when hover notifications
-- TODO: how to keep the notification


-- SOME SETTINGS
-- Timeout for notification priority
-- NOTE: set 0 for no timeout
local timeout_critical	= 0
local timeout_normal	= 5
local timeout_low		= 2

-- ===========================================

-- FIXME: remove me
naughty.config.defaults.position = settings.notification_position

ruled.notification.connect_signal('request::rules', function()
	-- General
	ruled.notification.append_rule {
		rule		= { },
		properties	= {
			screen			= awful.screen.focused,
			implicit_timeout= timeout_normal,
			hover_timeout	= timeout_critical,
			position		= settings.notification_position
		}
	}

	-- Urgent mode
	ruled.notification.append_rule {
		rule		= { urgency = 'critical' },
		properties	= {
			bg = beautiful.bg_focus,
			fg = beautiful.bg_normal,
			timeout = timeout_critical
		}
	}

	-- Normal mode
	ruled.notification.append_rule {
		rule		= { urgency = 'normal' },
		properties	= {
			bg = beautiful.bg_normal,
			fg = beautiful.fg_normal,
			timeout = timeout_normal
		}
	}

	-- Low mode
	ruled.notification.append_rule {
		rule		= { urgency = 'low' },
		properties	= {
			bg = beautiful.bg_normal,
			fg = beautiful.fg_minimize,
			timeout = timeout_low
		}
	}
end)

-- ===========================================

-- Connect signal
naughty.connect_signal('request::display', function(n)
	naughty.layout.box { notification = n }
end)
