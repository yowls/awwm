local ruled = require('ruled')

-- FLOATING MODE
-----------------------
-- set class|instance|name|role|type|etc window in floating mode

ruled.client.append_rule {
	id		= "floating",
	rule_any = {
		instance = {
			"DTA",		-- Firefox addon DownThemAll.
			"Biblioteca",
			"Library",
			"copyq",	-- Includes session name in class.
			"pinentry",
		},
		class = {
			"Arandr",
			"Wpa_gui",
			"veromix",
			"xtightvncviewer",
			"Blueman-manager",
			"Xephyr",
			"URxvt",
			"XTerm",
			"pcmanfm",
			"pcmanfm-qt",
			"Synaptic",
			"Gparted",
			"Pavucontrol",
			"vlc",
			"Gpick",
			"ark",
			"kcalc",
			"Kruler",
			"MessageWin",		-- kalarm.
			"Sxiv",
			"feh",
			"MEGAsync",
			"pcloud",
			"Lxappearance",
			"qt5ct",
			"Kvantum Manager",
			"Tor Browser"		-- avoid fingerprinting by screen size.
		},
		name = {
			"Event Tester",		-- xev
		},
		role = {
			"AlarmWindow",		-- Thunderbird's calendar.
			"ConfigManager",	-- Thunderbird's about:config.
			"pop-up"
		}
	},
	properties = {floating = true}
}
-- vim: tabstop=4:shiftwidth=4:softtabstop=0:noexpandtab:nowrap
