local ruled = require('ruled')

-- EXCEPTIONS
-----------------------
-- exclude instances/roles of window

ruled.client.append_rules {
	{
		rule = {class = "firefox"},
		except = {instance = "Browser"},
		properties = {floating = true},
	},
	{
		rule = {class = "firefox"},
		except = {instance = "Places"},
		properties = {floating = true},
	}
}
-- vim: tabstop=4:shiftwidth=4:softtabstop=0:noexpandtab:nowrap
