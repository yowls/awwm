local awful = require('awful')
local ruled = require("ruled")


-- RESET RULES
awful.rules.rules = {}


-- IMPORT MODULES
require('awful.autofocus')

ruled.client.connect_signal("request::rules", function()
	require('library.rules.global')
	require('library.rules.apps')
	require('library.rules.floating')
	require('library.rules.tag')
end)
-- vim: filetype=lua tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
