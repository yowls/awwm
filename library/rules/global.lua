local beautiful = require('beautiful')
local awful = require('awful')
local ruled = require('ruled')

local enable_titlebar = require("settings.theming").enable_titlebar
local ap = awful.placement


-- GENERAL RULES FOR ALL CLIENTS
ruled.client.append_rules {
	{	-- set properties for all windows
		id			= "global",
		rule		= { },	-- match all
		properties	= {
			border_width	= beautiful.border_width,	-- TODO: check is is redundant
			border_color	= beautiful.border_normal,	-- TODO: check is is redundant
			focus			= awful.client.focus.filter,
			screen			= awful.screen.focused,
			placement		= ap.no_overlap + ap.no_offscreen + ap.centered,
			raise			= true,
			switchtotag		= true
		}
	},
	{ -- show titlebars if is allowed
		rule_any	= {
			type = {"normal", "dialog"}
		},
		properties	= { titlebars_enabled = enable_titlebar }
	}
}
-- vim: tabstop=4:shiftwidth=4:softtabstop=0:noexpandtab:nowrap
