local awful		= require('awful')
local beautiful	= require('beautiful')
local gcolor	= require("gears.color")
local recolor	= gcolor.recolor_image

local helpers = {}		-- main table

-- LAYOUTS

function helpers.create_layouts(layout_names)
	local layout_list = {}
	for _,k in pairs(layout_names) do
		table.insert(layout_list, awful.layout.suit[k])
	end
	return layout_list
end


-- TAG ICON

function helpers.get_tag_icon(_, ntag)
	local tag_icons = beautiful.icons.taglist
	local tag_color = beautiful.fg_normal

	if ntag > 0 and ntag < 10 then
		local icon = "t" .. tostring(ntag)
		return recolor(tag_icons[icon], tag_color)
	else
		return recolor(tag_icons.t0, tag_color)
	end
end


return helpers
