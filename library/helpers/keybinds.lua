local client = client
local awful = require('awful')

local debug = require("library.helpers.debug")

local helpers = {}


--=======================================================
-- {{{ FOCUS
function helpers.focus_previous ()
	awful.client.focus.history.previous()
	if client.focus then
		client.focus:raise()
	end
end

-- TODO: toggle minimize
function helpers.minimize ()
	local c = awful.client.restore()
	if c then
		c:emit_signal( "request::activate", "key.unminimize", {raise = true})
	end
end

-- }}}


--=======================================================
-- {{{ TAGS
function helpers.tag_view (index)
	if index == 0 then
		index = 10
	end
	local screen = awful.screen.focused()
	local tag = screen.tags[index]
	if tag then
		tag:view_only()
	end
end

function helpers.tag_move_client (index)
	if client.focus then
		if index == 0 then
			index = 10
		end
		local tag = client.focus.screen.tags[index]
		if tag then
			client.focus:move_to_tag(tag)
		end
	end
end

function helpers.tag_toggle (index)
	if index == 0 then
		index = 10
	end
	local screen = awful.screen.focused()
	local tag = screen.tags[index]
	if tag then
		awful.tag.viewtoggle(tag)
	end
end

function helpers.tag_toggle_client (index)
	if client.focus then
		if index == 0 then
			index = 10
		end
		local tag = client.focus.screen.tags[index]
		if tag then
			client.focus:toggle_tag(tag)
		end
	end
end

-- }}}


--=======================================================
-- {{{ SPAWN
-- TODO: replace with connect_signal just for send notification
--[[ awesome.connect_signal('start::aplication', function(cmd)
	local test = "command -v " .. cmd
	awful.spawn.easy_async_with_shell(test, function(stdout)
		if not stdout then
			debug.notify("Command not found", cmd)
		end
	end)
end) --]]

-- }}}

return helpers
