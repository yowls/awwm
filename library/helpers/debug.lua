local naughty	= require('naughty')
local beautiful	= require('beautiful')
local gfs		= require('gears.filesystem')
-- local gdebug	= require('gears.debug')
local paths		= require("settings.paths")
local helpers	= {}		-- main table


--
-- NOTIFICATION
--
function helpers.notify(title, text, timeout, urgency)
	naughty.notification {
		icon	= beautiful.awesome_icon,
		title	= title,
		message	= text,
		timeout	= timeout or 5,
		urgency	= urgency or "normal"
	}
end

function helpers.notify_low_level(title, message)
	naughty.notification {
		title	= title,
		message	= message,
		urgency	= "critical"
	}
end

function helpers.notify_errors()
	naughty.connect_signal("request::display_error", function(message, startup)
		-- send notification
		local startup_state = "Error detected: "..(startup and " during startup!" or "!")
		helpers.notify_low_level(startup_state, message)

		-- log to file
		local log_file = paths.log_awesome_err
		local open_log = io.open(log_file, 'a')

		if not open_log then
			helpers.notify_low_level("Cannot open:", log_file)
			return
		end

		open_log:write(startup_state, "\n")
		open_log:write(message)
		open_log:write("\n\n==========================================\n\n")
		open_log:close()
	end)
end


--
-- WRITE LOG TO A FILE
--
function helpers.check_log_dir ()
	-- if dir doenst exist, create it
	local log_dir = paths.awesome_cache
	if not gfs.dir_readable(log_dir) then
		local sucess, msg = gfs.make_directories(log_dir)
		if not sucess then
			helpers.notify_low_level("Error creating log dir", msg)
		end
	end
end

function helpers.log(text)
	local log_file = paths.log_awesome_out
	local open_log = io.open(log_file, 'a')

	if not open_log then
		helpers.notify("Cannot open", log_file)
		return
	end

	local now = os.date("%Y-%m-%d |> %H_%M_%S")

	open_log:write("DATE: " .. now, "\n")
	open_log:write(text)
	open_log:write("\n\n==========================================\n\n")
	open_log:close()
end

function helpers.log_app(stdout, stderr)
	local log_file = paths.log_apps
	local open_log = io.open(log_file, "a")

	if not open_log then
		helpers.notify("Cannot open", log_file)
		return
	end

	open_log:write(stdout, "\n")

	if stderr then
		-- local title = '<b>Error</b>: ' .. app_command
		local title = '<b>Error</b>:'
		local msg = stderr:gsub('%\n', '')

		helpers.notify(title, msg, 10)

		open_log:write("\n")
		open_log:write(title, "\n")
		open_log:write(stderr, "\n")
	end

	open_log:write("\n==========================================\n\n")
	open_log:close()
end


return helpers
