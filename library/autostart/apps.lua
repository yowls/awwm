---
-- AUTOSTART APPLICATIONS
-- Run applications if is not runnning
---

local awful = require('awful')
local debug = require('library.helpers.debug')
local autostart = require("settings.autostart_apps")


-- Run a command and save the logs
local function run(props)
	local args = props or {}
	local cmd = args.cmd or args[1]
	local delay = args.delay or 0

	-- check if cmd exists
	local test = "command -v " .. cmd
	awful.spawn.easy_async_with_shell(test, function(stdout)
		if stdout then
			cmd = "sleep " .. tostring(delay) .. "; " .. cmd
			awful.spawn.easy_async_with_shell(cmd, debug.log_app)
		else
			debug.log_app("", "App not found: " .. cmd)
		end
	end)
end


-- Return true if is running else runit
local function running(app_name, props)
	local cmd = "pgrep -f " .. app_name
	awful.spawn.easy_async_with_shell(cmd, function (stdout)
		if stdout:gsub("%s+", "") == '' then
			run(props)
			return false
		end
		return true
	end)
end


-- MAIN
-- run essential programs if are not running
for process, command in pairs(autostart.system) do
	running(process, {command})
end

-- run applications only at start
if awesome.startup then
	for _,app in pairs(autostart.apps) do
		local process = app.ps or app[1]
		running(process, {
			cmd = app.cmd or process,
			delay = app.delay or 0
		})
	end
end

-- run scripts in a shell
for _,script in pairs(autostart.scripts) do
	awful.spawn.with_shell(script)
end
