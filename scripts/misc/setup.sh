#!/bin/sh

# Define colors
colorRed="\033[0;31m"
colorYellow="\033[0;33m"
colorGreen="\033[0;32m"
colorBlue="\033[0;34m"
colorGray="\033[0;37m"
colorEnd="\033[0m"


# Print help message
usage() {
	cat << EOF
Usage: $0 [-y | -b | -h ]

	-y                       | Just install missing packages
	-d                       | Dont ask
	-b [branch]              | Use another branch
	-h                       | Show help message
EOF
}


# Get parameters
while getopts "y:b:h" OPT
do
	case "$OPT" in
		y) # Install all missing packages
			echo "> bruh"
		;;
		b) # Use another branch
			echo "> Setting branch at: $colorYellow $OPTARG $colorEnd"
		;;
		h) # Display help
			echo "Install the Awesome window manager ready to use with my config"
			usage
			exit 0
		;;
		*) # In case of an incorrect parameter
			echo "$colorRed[ERROR]$colorEnd: Invalid parameter"
			usage
			exit 1
		;;
	esac
done

shift "$((OPTIND-1))"


# Print a welcome message
function welcome(){
	echo -e $colorYellow
	echo "  ______    __    ______    ______    "
	echo " /\  == \  /\ \  /\  ___\  /\  ___\   "
	echo " \ \  __<  \ \ \ \ \___  \ \ \  __\   "
	echo "  \ \_\ \_\ \ \_\ \/\_____\ \ \_____\ "
	echo "   \/_/ /_/  \/_/  \/_____/  \/_____/ "
	sleep 0.6	# a dramatic cut
	echo "  ______    __     __    ______    ______    ______    __    __    ______    "
	echo " /\  __ \  /\ \  _ \ \  /\  ___\  /\  ___\  /\  __ \  /\ \"-./  \  /\  ___\   "
	echo " \ \  __ \ \ \ \/ \".\ \ \ \  __\  \ \___  \ \ \ \/\ \ \ \ \-./\ \ \ \  __\   "
	echo "  \ \_\ \_\ \ \__/\".~\_\ \ \_____\ \/\_____\ \ \_____\ \ \_\ \ \_\ \ \_____\ "
	echo "   \/_/\/_/  \/_/   \/_/  \/_____/  \/_____/  \/_____/  \/_/  \/_/  \/_____/ "
	echo -e -n $colorEnd
	sleep 0.4
}


# TODO: re-check packages names
# Packages for each dist
declare -a pacman_pkg=()
declare -a apt_pkg=()
declare -a dnf_pkg=()
declare -a zypper_pkg=()
declare -a xbps_pkg=()
declare -a eopkg_pkg=()


# Check package manager for package suggestions
pm="?"
[ $(command -v pacman) ] && pm="pacman"
[ $(command -v apt) ] && pm="apt"
[ $(command -v dnf) ] && pm="dnf"
[ $(command -v zypper) ] && pm="zypper"
[ $(command -v xbps) ] && pm="xbps"
[ $(command -v eopkg) ] && pm="eopkg"
echo -e "$colorGreen{ Using $(uname -n) with $pm as package manager }$colorEnd"


# Suggest install dependencies
# TODO: update package manager database before
#       require super user
#       read, for example, pacman_pkg and try install element from array
function installPkg( pkg ){
	if [ pkg = "pacman" ]; then
		# print pending packages
		echo pacman_pkg
		# ask for install
		sudo pacman -S pacman_pkg

	elif [ pkg = "apt" ]; then
		# print pending packages
		echo apt_pkg
		# ask for install
		sudo apt install apt_pkg

	elif [ pkg = "dnf" ]; then
		# print pending packages
		echo dnf_pkg
		# ask for install

	elif [ pkg = "zypper" ]; then
		# print pending packages
		echo zypper_pkg
		# ask for install

	elif [ pkg = "xbps" ]; then
		# print pending packages
		echo xbps_pkg
		# ask for install

	elif [ pkg = "eopkg" ]; then
		# print pending packages
		echo eopkg_pkg
		# ask for install

	else
		echo "PKG not found"
	fi
}



# TODO: reference require libraries
# TODO: just ask to install all of required
#       and show which is already installed
#       jump to next step if all are installed
# Check require dependencies
function checkRequire() {
	echo -e "$colorBlue   I) Checking dependencies.. \t\t(1/4)$colorEnd"

	echo -e -n "\n Window Manager: Awesome"
	if [ $(command -v awesome) ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('awesome-git')
		apt_pkg+=('awesome')
		dnf_pkg+=('awesome')
		zypper_pkg+=('awesome')
		xbps_pkg+=('awesome')
		eopkg_pkg+=('awesome')
	fi

	echo -e -n "\n X11 Compositor: Picom"
	if [ $(command -v picom) ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('picom')
		apt_pkg+=('compton')
		dnf_pkg+=('picom')
		zypper_pkg+=('picom')
		xbps_pkg+=('picom')
		eopkg_pkg+=('picom')
	fi

	echo -e -n "\n Application launcher: Rofi"
	if [ $(command -v rofi) ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('rofi')
		apt_pkg+=('rofi')
		dnf_pkg+=('rofi')
		zypper_pkg+=('rofi')
		xbps_pkg+=('rofi')
		eopkg_pkg+=('rofi')
	fi

	# TODO: integrate xwinwrap
	# [ $(command -v xwinwrap) ] \

	echo -e -n "\n Fallback icon theme: Gnome Icon Theme"
	if [ -d /usr/share/icons/gnome ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('gnome-icon-theme')
		apt_pkg+=('gnome-icon-theme')
		dnf_pkg+=('gnome-icon-theme')
		zypper_pkg+=('gnome-icon-theme')
		xbps_pkg+=('gnome-icon-theme')
		eopkg_pkg+=('gnome-icon-theme')
	fi

	# --------------
	# Check if not all are installed and ask to install
	# Otherwise jump to the next step
	if [ pm = "pacman" ]; then
		if [ ${#pacman_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("pacman")
		fi

	elif [ pm = "apt" ]; then
		if [ ${#apt_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("apt")
		fi

	elif [ pm = "dnf" ]; then
		if [ ${#dnf_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("dnf")
		fi

	elif [ pm = "zypper" ]; then
		if [ ${#zypper_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("zypper")
		fi

	elif [ pm = "xbps" ]; then
		if [ ${#xbps_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("xbps")
		fi

	elif [ pm = "eopkg" ]; then
		if [ ${#eopkg_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("eopkg")
		fi

	else
		echo "Incorrect validation :|"
	fi


	# TODO: Reset array's
}


# TODO: ask which one would like to install
#       and show which is already installed in the right side
#       jump to next step if all are installed
# Check not so requiere but usefull dependencies
function checkOptional() {
	echo "Optional dependecies:"

	echo -e -n "$colorGray Maim $colorEnd (screenshot tool)"
	if [ $(command -v maim) ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('maim')
		apt_pkg+=('maim')
		dnf_pkg+=('maim')
		zypper_pkg+=('maim')
		xbps_pkg+=('maim')
		eopkg_pkg+=('maim')
	fi

	echo -e -n "$colorGray Redshift $colorEnd (night light)"
	if [ $(command -v redshift) ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('redshift')
		apt_pkg+=('redshift')
		dnf_pkg+=('redshift')
		zypper_pkg+=('redshift')
		xbps_pkg+=('redshift')
		eopkg_pkg+=('redshift')
	fi

	echo -e -n "$colorGray Playerctl $colorEnd (music control)"
	if [ $(command -v playerctl) ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('playerctl')
		apt_pkg+=('playerctl')
		dnf_pkg+=('playerctl')
		zypper_pkg+=('playerctl')
		xbps_pkg+=('playerctl')
		eopkg_pkg+=('playerctl')
	fi

	echo -e -n "$colorGray Xephyr $colorEnd (launch awesome in a nested session)"
	if [ $(command -v Xephyr) ]; then
		echo -e "\t $colorGreen [O] Installed $colorEnd"
	else
		echo -e "\t $colorRed [X] Not installed $colorEnd"
		pacman_pkg+=('xorg-server-xephyr')
		apt_pkg+=('xorg-server-xephyr')
		dnf_pkg+=('xorg-server-xephyr')
		zypper_pkg+=('xorg-server-xephyr')
		xbps_pkg+=('xorg-server-xephyr')
		eopkg_pkg+=('xorg-server-xephyr')
	fi

	# Laptop user
	# echo -e "\t [?] $colorGray Light $colorEnd (controls screen brightness)"
	# echo -e "\t [?] $colorGray TLP $colorEnd (optimize battery life)"

	# --------------
	# Check if not all are installed and ask to install
	# Otherwise jump to the next step
	if [ pm = "pacman" ]; then
		if [ ${#pacman_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("pacman")
		fi

	elif [ pm = "apt" ]; then
		if [ ${#apt_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("apt")
		fi

	elif [ pm = "dnf" ]; then
		if [ ${#dnf_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("dnf")
		fi

	elif [ pm = "zypper" ]; then
		if [ ${#zypper_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("zypper")
		fi

	elif [ pm = "xbps" ]; then
		if [ ${#xbps_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("xbps")
		fi

	elif [ pm = "eopkg" ]; then
		if [ ${#eopkg_pkg[@]} -eq 0 ]; then
			echo "All installed.. going to next step"
		else
			installPkg("eopkg")
		fi

	else
		echo "Incorrect validation :|"
	fi


	# TODO: Reset array's
}


# Check if there is a existent awesome wm config
function backup() {
	echo -e "$colorBlue III) Checking existent config.. \t(2/4)$colorEnd"

	if [ -d "$HOME/.config/awesome" ]; then
		echo -e "$colorRed [WARNING] $colorEnd Detecting existent awesome config \n"
		echo -e "+ Move the current Awesome config?"
		echo -e "\t $colorRed(~/.config/awesome)$colorEnd -> $colorGreen(~/.config/awesome-old)$colorEnd"
		echo -e "\n Proceed? $colorYellow(Y/n)$colorEnd"

		while [[ "$backup_opt" != "y" && "$backup_opt" != "Y" && "$backup_opt" != "n" ]]
		do
			read -p " => " backup_opt
			if [ $backup_opt = "y" ] || [ $backup_opt = "Y" ]; then
				# mv ~/.config/awesome ~/.awesome-old
				echo -e "* Done."

			elif [ $backup_opt = "n" ]; then
				echo -e "*$colorRed Directory conflict. Aborting$colorEnd"
				exit 1
			fi
		done

	else
		echo "There is no conflict between folders \n Continuing.."
	fi
}


# TODO: choose which branch to use
# Download the repository
function implement() {
	echo -e "$colorBlue IV) Going to integrate \t\t(3/4)$colorEnd"
	echo -e "+ Download the repository with: \n\t1) HTTPS $colorGray[default]$colorEnd \n\t2) SSH"
	echo -e "\n Option: (1 or 2)"
	read -r -p " => " clone_opt

	if [ $clone_opt -eq 2 ]; then
		echo -e "\n Cloning with$colorGreen SSH$colorEnd into ~/.config/awesome .."
		# git clone git@gitlab.com:yowls/awwm.git ~/.config/awesome
	else
		echo -e "\n * Cloning with$colorGreen HTTPS$colorEnd into ~/.config/awesome .."
		# git clone --depth 1 https://gitlab.com/yowls/awwm/ ~/.config/awesome
	fi
}


# End message
function post_install() {
	echo -e "$colorBlue  V) Post Install \t\t(4/4)$colorEnd"
	echo -e "$colorGreen All ready.$colorEnd \nNow log off and login using awesome session"
	sleep 0.7
	echo "(if you are using a Display Manager)"
	sleep 0.5
	echo "(~laughs in xinitrc~)"
}


# Launch Xephyr
function testing() {
	echo -e "$colorYellow ------------------------------- $colorEnd"
	echo "$colorBlue Try it out now in Xephyr ?$colorRed (y/N)$colorEnd"
	read -r -p " => " test_opt
	if [ $test_opt -eq "y" || $test_opt -eq "Y" ]; then
		bash $HOME/.config/awesome/scripts/awesome-xephyr
	fi
}


# -----------------------
welcome
echo -e $colorYellow
printf "%$(tput cols)s" " " | tr " " "_"
echo -e $colorEnd

# step 1
checkRequire
checkOptional
echo -e $colorYellow
printf "%$(tput cols)s" " " | tr " " "_"
echo -e $colorEnd

# step 2
backup
echo -e $colorYellow
printf "%$(tput cols)s" " " | tr " " "_"
echo -e -n $colorEnd

# step 3
implement
echo -e $colorYellow
printf "%$(tput cols)s" " " | tr " " "_"
echo -e $colorEnd

# step 4
post_install
testing
