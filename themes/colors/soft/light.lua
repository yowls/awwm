-- SOFT
-- light mode

return {
	-- Background
	bg	= "#475C6C",
	bgi	= "#475C6C" .. "88",	-- inactive

	-- Foreground
	fg	= "#FAF9F9",
	fgi	= "#6C757D",	-- inactive

	-- Special
	accent	= "#A89FE9",
	alert	= "#F7B7D9",

	-- Palette
	color1	= "#E9A9E7",	-- red
	color2	= "#B7E4C7",	-- green
	color3	= "#F7E8D2",	-- yellow
	color4	= "#B8CCF8",	-- blue
	color5	= "#A89FE9",	-- magenta
	color6	= "#FBC8D4" 	-- cyan
}
