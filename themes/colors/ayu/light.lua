-- AYU COLOR SCHEME
-- light mode

return {
	-- Background
	bg	= "#FAFAFA",
	bgi	= "#FAFAFA" .. "88",	-- inactive

	-- Foreground
	fg	= "#575F66",
	fgi	= "#ABB0B6",	-- inactive

	-- Special
	accent	= "#FF9940",
	alert	= "#F51818",

	-- Palette
	color1	= "#F07171",	-- red
	color2	= "#4CBF99",	-- green
	color3	= "#F2AE49",	-- yellow
	color4	= "#399EE6",	-- blue
	color5	= "#A37ACC",	-- magenta
	color6	= "#E6BA7E" 	-- cyan
}
