-- MOONY
-- dark mode

return {
	-- Background
	bg	= "#555b6e",
	bgi	= "#555b6e" .. "88",	-- inactive

	-- Foreground
	fg	= "#ffffff",
	fgi	= "#e0e1dd",	-- inactive

	-- Special
	accent	= "#e0c3fc",
	alert	= "#c05299",

	-- Palette
	color1	= "#dab6fc",	-- red
	color2	= "#cbb2fe",	-- green
	color3	= "#bbadff",	-- yellow
	color4	= "#9fa0ff",	-- blue
	color5	= "#8e94f2",	-- magenta
	color6	= "#757bc8" 	-- cyan
}
