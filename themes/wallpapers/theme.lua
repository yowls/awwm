---
-- SET THEME SETTINGS FOR NAUGHTY NOTIFICATION
---

local paths = require('settings.paths')

local append_theme = function ()
	return {
		wallpaper = paths.awesome_themes .. "wallpapers/space_simple.png"
	}
end

return append_theme
