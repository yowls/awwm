local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

-- IMPORT WIDGETS
local widgets_path = "themes.docks.Bottom.widgets."
local apps		= require(widgets_path .. 'apps')()
local folders		= require(widgets_path .. 'folders')()
local separator		= require(widgets_path .. 'separator')()
-- local wrap		= require(widgets_path .. 'wrap')()

-- TODO: add indicator if is running
-- TODO: add connect_signal mouse enter for reset the timer
-- TODO: add connect_signal mouse leave for autohide this dock


-- CREATE TABLE OF WIDGETS
local init_widgets = function()

	-- Set shape of the dock
	local popup_shape = function(cr, width, height)
		gears.shape.rounded_rect(cr, width, height, 2)
	end

	-- Set widgets of the dock
	local widgets = {
		{
			-- widgets
			apps,
			separator,
			folders,

			-- properties
			layout = wibox.layout.fixed.horizontal
		},
		shape	= popup_shape,
		bg	= beautiful.bg_normal,
		widget	= wibox.container.background
	}

	return widgets
end


-- SET MAIN STATUS BAR
local dock = function(s)

	-- Settings
	local height	= 40	-- in pixels
	local offset	= 15	-- in pixels

	-- Add widgets
	local dock_widgets = init_widgets()

	-- Create the wibox bar
	local bar = awful.popup {
		screen		= s,
		visible		= true,
		ontop		= true,
		type		= "dock",
		height		= dpi(height),
		maximum_height	= dpi(height),
		placement	= awful.placement.bottom,
		bg		= beautiful.bg_normal,
		fg		= beautiful.fg_normal,
		shape		= gears.shape.rectangle,
		widget		= dock_widgets
	}

	-- Reserve space in the screen
	bar:struts { bottom = dpi(height) }

	return bar
end

return dock
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
