---
-- Create a dock bar using
-- awful popup lib for dynamic size
---

local dock_path	= "themes.docks.Bottom."
local settings	= require('settings.theming')


-- SET THE DOCK BAR
screen.connect_signal("request::desktop_decoration", function(s)
	if settings.dock_screen == "all" then
		-- Apply dock for all screens
		if settings.dock_mode == "compact" then
			s.dock	= require(dock_path .. "compact")(s)
		else
			s.dock	= require(dock_path .. "extended")(s)
		end

	elseif s.index == settings.dock_screen then
		-- Apply dock for only one screen
		if settings.dock_mode == "compact" then
			s.dock	= require(dock_path .. "compact")(s)
		else
			s.dock	= require(dock_path .. "extended")(s)
		end
	end

	-- Activator
	-- s.bait_dock		= require('themes.docks.Bottom.bait')(s)

end)
