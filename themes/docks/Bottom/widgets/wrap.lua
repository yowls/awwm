local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi


-- MAIN
local wrapper = function(widget)
	-- Wrap widget into a container
	local container = wibox.widget {
		{
			widget,
			bg	= beautiful.bg .. "99",
			shape = function(cr, width, height)
				gears.shape.rounded_rect(cr, width, height, dpi(4))
			end,
			widget = wibox.container.background
		},
		top		= dpi(10),
		bottom	= dpi(10),
		widget	= wibox.container.margin
	}

	-- Mouse hovers on the widget
	container:connect_signal('mouse::enter', function()
		container.bg = beautiful.fg .. "44"
	end)

	-- Mouse leaves the widget
	container:connect_signal('mouse::leave', function()
		container.bg = beautiful.bg
	end)

	-- Mouse pressed the widget
	container:connect_signal('button::press', function()
			container.bg = beautiful.accent .. "99"
	end)

	-- Mouse releases the widget
	container:connect_signal('button::release', function()
			container.bg = beautiful.accent
	end)

	return container
end

return wrapper
