local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local icons = beautiful.icons.applications

-- TODO: set rise_or_spawn instead of spawn (from helpers.keybinds?)
-- TODO: create right click button popup for {generics, specials}
--         generic: new, close, kill, etc
--         specials: new private window for firefox


-- FIXME: Broken pipe; Command find write error
-- Find icon theme in global_icon
-- if cannot find it, return icon of error
local function find_icon (name)
	local cmd = string.format("find /usr/share/icons/%s -type f,l | grep -w %s.svg -m 1", beautiful.icon_theme, name)
	local output = io.popen(cmd)
	local dirty_output = output:read("*a")
	output:close()

	if dirty_output == "" then
		return icons["unknown"]
	end

	return string.gsub(dirty_output, "\n", "")
end


-- MAIN
local template = function(app_name, app_cmd, app_icon)
	local icon = find_icon(app_icon)

	-- widget template
	local widget = wibox.widget {
		{
			id		= "icon",
			resize	= true,
			image	= icon,
			widget	= wibox.widget.imagebox
		},
		valigh = 'center',
		layout = wibox.container.place
	}

	-- widget mouse button actions
	widget:buttons(gears.table.join(
		awful.button (
			{},1,
			nil,
			function() awful.spawn(app_cmd) end
		)
	))

	-- mouse hover popup
	awful.tooltip {
		objects = { widget },
		text = app_name,
		mode = 'outside',
		align = 'right',
		margin_leftright = dpi(8),
		margin_topbottom = dpi(8),
		preferred_positions = {'top', 'bottom', 'right', 'left'}
	}

	return widget
end

return template
