---
-- Create a list of folder widgets
-- based on user settings (settings/dock_apps)
---

local wibox = require('wibox')

-- IMPORT THE TEMPLATE FOR CREATE WIDGETS
local base	= require('themes.docks.Bottom.widgets.folders.base')
local fav	= require('settings.dock_apps').folders


-- MAIN
local widget = function()
	local folders = {
		layout = wibox.layout.fixed.horizontal
	}

	for _,name in pairs(fav) do
		local one_widget = base(name[1], name[2], name[3])
		table.insert(folders, one_widget)
	end

	-- return a table with widgets
	return wibox.widget {
		folders,
		layout = wibox.layout.align.horizontal
	}
end

return widget
