local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

-- IMPORT WIDGETS
local widgets_path = "themes.docks.Bottom.widgets."
local apps		= require(widgets_path .. 'apps')()

-- TODO: add indicator if the app is running
-- TODO: add connect_signal mouse enter for reset the timer
-- TODO: add connect_signal mouse leave for autohide this dock


-- CREATE TABLE OF WIDGETS
local init_widgets = function()

	-- Set shape of the dock
	local popup_shape = function(cr, width, height)
		gears.shape.rounded_rect(cr, width, height, 2)
	end

	-- Set widgets of the dock
	local widgets = {
		{
			-- widgets
			apps,

			-- properties
			layout = wibox.layout.fixed.horizontal
		},
		shape	= popup_shape,
		bg	= beautiful.bg_normal,
		widget	= wibox.container.background
	}

	return widgets
end


-- FIXME: add a vertical offset
-- SET MAIN STATUS BAR
local dock = function(s)

	-- Settings
	local height	= 40	-- in pixels
	local offset_v	= 15	-- in pixels

	-- Add widgets
	local dock_widgets = init_widgets()

	-- Create the bar with awful popup for dynamic size
	local bar = awful.popup {
		screen		= s,
		visible		= true,
		ontop		= true,
		type		= "dock",
		height		= dpi(height),
		maximum_height	= dpi(height),
		placement	= awful.placement.bottom,
		bg		= beautiful.bg_normal,
		fg		= beautiful.fg_normal,
		shape		= gears.shape.rectangle,
		widget		= dock_widgets
	}

	-- Reserve space in the screen
	bar:struts { bottom = dpi(height) }

	return bar
end

return dock
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
