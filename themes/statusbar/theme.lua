---
-- SET GENERAL THEME SETTINGS FOR STATUSBAR
---

local recolor = require("gears.color").recolor_image
local settings = require('settings.theming')

local append_theme = function ( theme )
	-- shortcuts
	local cs = theme.cs
	local icons = theme.icons

	return {
		statusbar_font		= settings.statusbar_font,

		-- WIBAR
		wibar_border_color	= cs.color2,
		wibar_fg			= cs.fg,
		wibar_bg			= cs.bg,
		-- wibar_bgimage		= ""

		-- MENUBAR PROMPT
		menubar_fg_normal		= cs.fg,
		menubar_bg_normal		= cs.bg,
		menubar_border_color	= cs.accent,
		menubar_border_width	= theme.border_width,

		-- TAGLIST
		taglist_fg_focus		= cs.bg,
		taglist_fg_occupied		= cs.color4,
		taglist_fg_urgent		= cs.alert,
		taglist_fg_empty		= cs.fgi,
		taglist_font			= settings.statusbar_font,
		taglist_squares_sel			= icons.taglist.busy,
		taglist_squares_unsel		= icons.taglist.busy,
		taglist_squares_sel_empty	= icons.taglist.empty,
		taglist_squares_unsel_empty	= icons.taglist.empty,
		taglist_squares_resize		= true,

		-- LAYOUTBOX
		layout_max			= recolor(icons.layout.max, cs.fg),
		layout_fullscreen	= recolor(icons.layout.fullscreen, cs.fg),
		layout_magnifier	= recolor(icons.layout.magnifier, cs.fg),
		layout_tile			= recolor(icons.layout.tile, cs.fg),
		layout_tileleft		= recolor(icons.layout.tile_left, cs.fg),
		layout_tilebottom	= recolor(icons.layout.tile_bottom, cs.fg),
		layout_tiletop		= recolor(icons.layout.tile_top, cs.fg),
		layout_fairv		= recolor(icons.layout.fair_vertical, cs.fg),
		layout_fairh		= recolor(icons.layout.fair_horizontal, cs.fg),
		layout_spiral		= recolor(icons.layout.spiral, cs.fg),
		layout_dwindle		= recolor(icons.layout.spiral_dwindle, cs.fg),
		layout_cornernw		= recolor(icons.layout.corner_nw, cs.fg),
		layout_cornerne		= recolor(icons.layout.corner_ne, cs.fg),
		layout_cornersw		= recolor(icons.layout.corner_sw, cs.fg),
		layout_cornerse		= recolor(icons.layout.corner_se, cs.fg),
		layout_floating		= recolor(icons.layout.floating, cs.fg),
	}
end

return append_theme
