local awful = require('awful')
local wibox = require('wibox')
local beautiful = require('beautiful')
local icons = beautiful.icons.system
local watch = awful.widget.watch
local recolor = require("gears.color").recolor_image

-- TODO: add mouse buttons
-- TODO: add tooltip
-- TODO: add popup


-- MAIN
local widget = {}
local function ram(args)
	-- Settings
	local settings	= args or {}
	local font		= settings.font		or beautiful.statusbar_font
	local refresh	= settings.refresh	or 10

	-- =======================================

	-- Create the widget
	local widget_icon = wibox.widget {
		{
			id = "icon",
			widget = wibox.widget.imagebox,
			image = recolor(icons.memory, beautiful.cs.fg),
			resize = true
		},
		valigh = 'center',
		layout = wibox.container.place
	}

	local widget_text = wibox.widget {
		widget = wibox.widget.textbox,
		font = font
	}

	widget = wibox.widget {
		widget_icon,
		widget_text,
		layout = wibox.layout.fixed.horizontal
	}

	-- =======================================

	local function get_status (w, stdout)
		-- clean output
		-- output: total, used, free, shared, buff_cache, available
		local _, used, _, _, _, _ =
			stdout:match('(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*')

		local usage
		if tonumber(used) < 1000 then
			usage = used .. " MB"
		else
			usage = string.format("%.2f", used/1000) .. " GB"
		end

		-- set widget text
		w.text = usage

		collectgarbage('collect')
	end

	-- update widget
	local shell_cmd = "sh -c 'free --mega | grep Mem:'"
	watch(shell_cmd, refresh, get_status, widget_text)

	return widget
end


-- Return widget
return setmetatable(widget, {
	__call = function(_, ...)
		return ram(...)
	end
})
