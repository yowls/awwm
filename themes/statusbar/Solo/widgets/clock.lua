local wibox		= require('wibox')
local gcolor	= require('gears.color')
local beautiful	= require('beautiful')


local widget = function()
	local icon = beautiful.icons.system.clock

	return wibox.widget {
		{
			{
				id		= "icon",
				resize	= true,
				image	= gcolor.recolor_image(icon, beautiful.cs.fg),
				widget	= wibox.widget.imagebox
			},
			valigh = 'center',
			layout = wibox.container.place
		},
		{
			font	= beautiful.statusbar_font,
			format	= '%H:%M',
			widget	= wibox.widget.textclock
		},
		layout = wibox.layout.fixed.horizontal
	}
end

return widget
