local awful = require('awful')
local gears = require('gears')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi	= beautiful.xresources.apply_dpi

local tasklist = {}

-- TODO: add popup
-- TODO: add tooltip


-- BUTTONS FUNCTIONS
tasklist.buttons = gears.table.join(
	awful.button({ }, 1,
		function (c)
			c:activate { context = "tasklist", action = "toggle_minimization" }
		end
	),
	awful.button({ }, 3,
		function()
			awful.menu.client_list { theme = { width = 250 } }
		end
	),
	awful.button({ }, 4,
		function()
			awful.client.focus.byidx(-1)
		end
	),
	awful.button({ }, 5,
		function()
			awful.client.focus.byidx( 1)
		end
	)
)


-- {{{ STYLES
tasklist.style = {
	shape  = gears.shape.rounded_rect,
	shape_border_width = dpi(2),
	shape_border_color = beautiful.bg_focus,
}
-- }}}


-- {{{ LAYOUTS
tasklist.layout = {
	spacing = dpi(20),
	spacing_widget = {
		{
			forced_width = dpi(5),
			shape	= gears.shape.circle,
			widget	= wibox.widget.separator
		},
		valign = 'center',
		halign = 'center',
		widget = wibox.container.place,
	},
	layout = wibox.layout.fixed.horizontal
}
-- }}}


-- {{{ TEMPLATE
tasklist.template = {
	{
		{
			{
				{
					id		= 'icon_role',
					widget	= wibox.widget.imagebox
				},
				margins	= dpi(2),
				widget	= wibox.container.margin
			},
			{
				id		= 'text_role',
				widget	= wibox.widget.textbox
			},
			layout = wibox.layout.fixed.horizontal
		},
		left	= dpi(15),
		right	= dpi(15),
		widget	= wibox.container.margin
	},
	id		= 'background_role',
	widget	= wibox.container.background
}
-- }}}


-- {{{ MAIN
local widget = function(s)
	return awful.widget.tasklist {
		screen		= s,
		filter		= awful.widget.tasklist.filter.currenttags,
		buttons		= tasklist.buttons,
		style		= tasklist.style,
		layout		= tasklist.layout,
		widget_template = tasklist.template
	}
end
return widget
-- }}}
