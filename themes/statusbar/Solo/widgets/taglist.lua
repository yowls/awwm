local awful = require('awful')
local gears = require('gears')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi	= beautiful.xresources.apply_dpi
local icons	= beautiful.icons.taglist
local cs	= beautiful.cs

local taglist = {}

-- TODO: add popup
-- TODO: add tooltip


-- {{{ MOUSE BUTTONS
taglist.buttons = gears.table.join(
	awful.button({ }, 1,
		function(t)
			t:view_only()
		end
	),
	awful.button({ }, 2,
		function(t)
			if client.focus then client.focus:move_to_tag(t) end
		end
	),
	awful.button({ }, 3,
		awful.tag.viewtoggle
	),
	awful.button({M}, 3,
		function(t)
			if client.focus then client.focus:toggle_tag(t) end
		end
	),
	awful.button({ }, 4,
		function(t)
			awful.tag.viewprev(t.screen)
		end
	),
	awful.button({ }, 5,
		function(t)
			awful.tag.viewnext(t.screen)
		end
	)
)
-- }}}


-- {{{ STYLES
-- taglist.style = {
--     shape = gears.shape.rounded_rect
-- }
-- }}}


-- {{{ TEMPLATE
local function update_callback (self, c3)
	-- Update icon
	local tag_icon = self:get_children_by_id('icon')[1]

	if c3.selected then
		tag_icon.image = gears.color.recolor_image(icons.focus, cs.fg)

	elseif #c3:clients() == 0 then
		tag_icon.image = gears.color.recolor_image(icons.unfocus, cs.fg)

	else
		tag_icon.image = gears.color.recolor_image(icons.selected, cs.fgi)
	end
end

local function create_callback (self, c3)
	-- Update all icons when start
	update_callback(self, c3)

	-- Mouse hover
	self:connect_signal('mouse::enter', function()
		local tag_icon = self:get_children_by_id('icon')[1]
		tag_icon.image = gears.color.recolor_image(icons.focus, cs.accent)
	end)

	-- Mouse leave
	self:connect_signal('mouse::leave', function()
		update_callback(self, c3)
	end)
end

-- NOTE: this will not use tag icon setting in tag_layout
taglist.template = {
	{
		id		= 'icon',
		image	= gears.color.recolor_image(icons.unfocus, cs.fg),
		widget	= wibox.widget.imagebox,
	},
	left	= dpi(1),
	right	= dpi(1),
	widget	= wibox.container.margin,
	create_callback = function(self, c3, index, objects) --luacheck: no unused args
		create_callback(self, c3)
	end,
	update_callback = function(self, c3, index, objects) --luacheck: no unused args
		update_callback(self, c3)
	end
}
-- }}}


-- {{{ MAIN
local widget = function(s)
	return awful.widget.taglist {
		screen		= s,
		filter		= awful.widget.taglist.filter.all,
		buttons		= taglist.buttons,
		-- style		= taglist.style,
		layout		= wibox.layout.fixed.horizontal,
		widget_template = taglist.template,
	}
end
return widget
-- }}}
