local naughty = require("naughty")
local ruled = require("ruled")
local wibox = require("wibox")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

local notification = {}

-- TODO: define notification shape
-- TODO: define buttons action
-- TODO: add more separation between notifications


-- TEMPLATE
notification.template = {
	{
		{
			{
				{ -- icon
					naughty.widget.icon,
					forced_height = 48,
					halign        = 'center',
					valign        = 'center',
					widget        = wibox.container.place
				},
				{ -- title
					align  = 'center',
					widget = naughty.widget.title,
				},
				{ -- message
					align  = 'center',
					widget = naughty.widget.message,
				},
				{ -- separator
					orientation   = 'horizontal',
					widget        = wibox.widget.separator,
					forced_height = dpi(1),
				},
				spacing = dpi(10),
				layout  = wibox.layout.fixed.vertical,
			},
			margins = dpi(5),
			widget  = wibox.container.margin,
		},
		id     = 'background_role',
		widget = naughty.container.background,
	},
	strategy = 'max',
	width    = dpi(160),
	widget   = wibox.container.constraint,
}


-- BIND ALL
ruled.notification.connect_signal('request::rules', function()
	ruled.notification.append_rule {
		rule		= {  },
		properties	= {
			widget_template	= notification.template,
			-- shape			= notification.shape
			margin			= dpi(4),
			border_width	= dpi(3)
		}
	}
end)
