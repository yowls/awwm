local gtable = require('gears.table')
local theme = {}


----------------------
--  LOAD ICON THEME --
----------------------
local icon_theme = require('themes.icons.theme')()
theme.icons = icon_theme	-- save icons


-----------------------
-- LOAD COLOR SCHEME --
-----------------------
local color_theme = require('themes.colors.theme')()
theme.cs = color_theme		-- save colors


-------------------------
-- GENERAL PREFERENCES --
-------------------------
local general_theme = require('themes.theme')(theme)

-- {{{ TODO: set others:
-- tasklist
-- mouse_finder
-- }}}


------------------
-- NOTIFICATION --
------------------
local notification_theme = require('themes.notifications.theme')(theme)


--------------
-- TITLEBAR --
--------------
local titlebar_theme = require('themes.titlebar.theme')(theme)


---------------
-- STATUSBAR --
---------------
local statusbar_theme = require('themes.statusbar.theme')(theme)


----------------
--	WALLPAPER --
----------------
-- (using as fallback mode)
local wallpaper_theme = require('themes.wallpapers.theme')()


-------------------
-- APPEND THEMES --
-------------------
theme = gtable.join(
	theme,
	general_theme,
	notification_theme,
	titlebar_theme,
	statusbar_theme,
	wallpaper_theme
)
return theme
