---
-- SET THEME SETTINGS FOR NAUGHTY NOTIFICATION
---

local settings = require('settings.theming')

local append_theme = function ()
	-- Load a random icon theme if the option is set to true
	local icon
	if settings.random_icons then
		icon = settings.icon_themes[ math.random(#settings.icon_themes) ]
	else
		icon = settings.icon
	end

	return require("themes.icons." .. icon)
end

return append_theme
