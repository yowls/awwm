---
-- SET GENERAL THEME SETTINGS FOR TITLEBAR
---

local recolor = require("gears.color").recolor_image
local settings = require('settings.theming')

local append_theme = function ( theme )
	-- shortcuts
	local cs = theme.cs
	local icons = theme.icons.titlebar

	return {
		titlebar_font		= settings.titlebar_font,

		titlebar_fg_normal	= cs.fg,
		titlebar_fg_focus	= cs.fg,
		titlebar_bg_normal	= cs.bgi,
		titlebar_bg_focus	= cs.bg,
		-- titlebar_bgimage_normal = "",
		-- titlebar_bgimage_focus  = "",

		-- CLOSE BUTTON
		titlebar_close_button_focus			= recolor(icons.close.normal, cs.color1),
		titlebar_close_button_focus_hover	= recolor(icons.close.hover, cs.color1),
		titlebar_close_button_focus_press	= recolor(icons.close.normal, cs.color2),
		titlebar_close_button_normal		= recolor(icons.close.normal, cs.fgi),
		titlebar_close_button_normal_hover	= recolor(icons.close.hover, cs.color1),
		titlebar_close_button_normal_press	= recolor(icons.close.normal, cs.fgi),
		-- FLOATING BUTTON
		titlebar_floating_button_focus_inactive			= recolor(icons.floating.normal, cs.fg),
		titlebar_floating_button_focus_inactive_hover	= recolor(icons.floating.hover, cs.color2),
		titlebar_floating_button_focus_inactive_press	= recolor(icons.floating.normal, cs.color2),
		titlebar_floating_button_focus_active			= recolor(icons.floating.normal, cs.color2),
		titlebar_floating_button_focus_active_hover		= recolor(icons.floating.hover, cs.color2),
		titlebar_floating_button_focus_active_press		= recolor(icons.floating.normal, cs.color3),
		titlebar_floating_button_normal_inactive		= recolor(icons.floating.normal, cs.fgi),
		titlebar_floating_button_normal_inactive_hover	= recolor(icons.floating.hover, cs.color2),
		titlebar_floating_button_normal_inactive_press	= recolor(icons.floating.normal, cs.color2),
		titlebar_floating_button_normal_active			= recolor(icons.floating.normal, cs.fgi),
		titlebar_floating_button_normal_active_hover	= recolor(icons.floating.hover, cs.color2),
		titlebar_floating_button_normal_active_press	= recolor(icons.floating.normal, cs.color2),
		-- MINIMIZE BUTTON
		titlebar_minimize_button_focus			= recolor(icons.minimize.normal, cs.fg),
		titlebar_minimize_button_focus_hover	= recolor(icons.minimize.hover, cs.color3),
		titlebar_minimize_button_focus_press	= recolor(icons.minimize.normal, cs.color3),
		titlebar_minimize_button_normal			= recolor(icons.minimize.normal, cs.fgi),
		titlebar_minimize_button_normal_hover	= recolor(icons.minimize.hover, cs.color3),
		titlebar_minimize_button_normal_press	= recolor(icons.minimize.normal, cs.color3),
		-- MAXIMIZED BUTTON
		titlebar_maximized_button_focus_inactive		= recolor(icons.maximized.normal, cs.fg),
		titlebar_maximized_button_focus_inactive_hover	= recolor(icons.maximized.hover, cs.color4),
		titlebar_maximized_button_focus_inactive_press	= recolor(icons.maximized.normal, cs.color4),
		titlebar_maximized_button_focus_active			= recolor(icons.maximized.normal, cs.color4),
		titlebar_maximized_button_focus_active_hover	= recolor(icons.maximized.hover, cs.color5),
		titlebar_maximized_button_focus_active_press	= recolor(icons.maximized.normal, cs.color5),
		titlebar_maximized_button_normal_inactive		= recolor(icons.maximized.normal, cs.fgi),
		titlebar_maximized_button_normal_inactive_hover	= recolor(icons.maximized.hover, cs.color5),
		titlebar_maximized_button_normal_inactive_press	= recolor(icons.maximized.normal, cs.color4),
		titlebar_maximized_button_normal_active			= recolor(icons.maximized.normal, cs.fgi),
		titlebar_maximized_button_normal_active_hover	= recolor(icons.maximized.hover, cs.color4),
		titlebar_maximized_button_normal_active_press	= recolor(icons.maximized.normal, cs.color4),
		-- ONTOP BUTTON
		titlebar_ontop_button_focus					= recolor(icons.top.normal, cs.fg),
		titlebar_ontop_button_focus_inactive		= recolor(icons.top.normal, cs.fg),
		titlebar_ontop_button_focus_inactive_hover	= recolor(icons.top.hover, cs.color5),
		titlebar_ontop_button_focus_inactive_press	= recolor(icons.top.normal, cs.color5),
		titlebar_ontop_button_focus_active			= recolor(icons.top.normal, cs.color5),
		titlebar_ontop_button_focus_active_hover	= recolor(icons.top.hover, cs.color5),
		titlebar_ontop_button_focus_active_press	= recolor(icons.top.normal, cs.color6),
		titlebar_ontop_button_normal				= recolor(icons.top.normal, cs.fgi),
		titlebar_ontop_button_normal_inactive		= recolor(icons.top.normal, cs.fgi),
		titlebar_ontop_button_normal_inactive_hover	= recolor(icons.top.hover, cs.color5),
		titlebar_ontop_button_normal_inactive_press	= recolor(icons.top.normal, cs.color5),
		titlebar_ontop_button_normal_active			= recolor(icons.top.normal, cs.color6),
		titlebar_ontop_button_normal_active_hover	= recolor(icons.top.hover, cs.color6),
		titlebar_ontop_button_normal_active_press	= recolor(icons.top.normal, cs.color6),
		-- STICKY BUTTON
		titlebar_sticky_button_focus				= recolor(icons.sticky.normal, cs.fg),
		titlebar_sticky_button_focus_inactive		= recolor(icons.sticky.normal, cs.fg),
		titlebar_sticky_button_focus_inactive_hover	= recolor(icons.sticky.hover, cs.color6),
		titlebar_sticky_button_focus_inactive_press	= recolor(icons.sticky.normal, cs.color6),
		titlebar_sticky_button_focus_active			= recolor(icons.sticky.normal, cs.color6),
		titlebar_sticky_button_focus_active_hover	= recolor(icons.sticky.hover, cs.color6),
		titlebar_sticky_button_focus_active_press	= recolor(icons.sticky.normal, cs.color5),
		titlebar_sticky_button_normal				= recolor(icons.sticky.normal, cs.fgi),
		titlebar_sticky_button_normal_inactive		= recolor(icons.sticky.normal, cs.fgi),
		titlebar_sticky_button_normal_inactive_hover= recolor(icons.sticky.hover, cs.color5),
		titlebar_sticky_button_normal_inactive_press= recolor(icons.sticky.normal, cs.color5),
		titlebar_sticky_button_normal_active		= recolor(icons.sticky.normal, cs.color5),
		titlebar_sticky_button_normal_active_hover	= recolor(icons.sticky.hover, cs.color5),
		titlebar_sticky_button_normal_active_press	= recolor(icons.sticky.normal, cs.color5),
	}
end

return append_theme
