### 📖 Description
This is my collection of themes for the built-in awesome titlebar<br>
The idea is to make it as modular as possible,<br>
to be able to import any theme easily

👀 Previews will be here
