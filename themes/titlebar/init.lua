---
-- LOAD TITLEBAR THEME
---

local settings = require('settings.theming')

if settings.enable_titlebar then
	local theme

	if settings.random_titlebar then
		theme = settings.titlebar_themes[ math.random(#settings.titlebar_themes) ]
	else
		theme = settings.titlebar
	end

	require('themes.titlebar.' .. theme)
end
