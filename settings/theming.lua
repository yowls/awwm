local theming = {}

--=======================================================
--    ⚙️ GENERAL
------------------------
-- Set font and size
-- default: sans 8
theming.global_font	= "Nevis 10"

theming.window_margin	= 6	-- gap between windows (px)
theming.window_border	= 3	-- border width (px)


--=======================================================
--    🍱 ICONS
-----------------------
-- Randomize theme from the list below
-- value: [true | false]
theming.random_icons = false

-- List of available theme names
theming.icon_themes = {
	"Min",		-- 1: TODO
}

-- Set icon theme for Awesome
-- value: string name from above or array item
theming.icon = "Min"

-- Set generic icon theme for applications
-- value: from /usr/share/icons/
theming.global_icon = "Papirus"


--=======================================================
--    🎨 COLOR SCHEME
------------------------
-- Randomize the color scheme on every restart
-- value: [true | false]
theming.random_color_scheme = false

-- List of available theme names
theming.color_scheme_themes = {
	"ayu",		-- 1 : ayu color scheme
	"moony",	-- 2 : soft and dark colors
	"minla",	-- 3 : TODO
	"soft"		-- 4 : TODO
}

-- Set color scheme
-- value: string name from above or array item
theming.color_scheme = "ayu"

-- Color scheme mode
-- value: "dark", "light"
theming.color_scheme_mode = "dark"


--=======================================================
--    🗨️ NOTIFICATIONS
-----------------------
-- Randomize theme on every restart
-- value: [true | false]
theming.random_notification = false

-- List of available theme names
theming.notification_themes = {
	"Solo",		-- 1: default
}

-- Set notification theme
-- value: string name from above or array item
theming.notification = "Solo"

-- Set notification font
theming.notification_font = "manrope light 10"


--=======================================================
--    🪂 TITLE BAR
------------------------
-- Do you like titlebars?
-- value: [true | false]
theming.enable_titlebar = true

-- Randomize theme on every restart
-- value: [true | false]
theming.random_titlebar = false

-- List of available theme names
theming.titlebar_themes = {
	"Solo"		-- 1: Default
}

-- Set titlebar theme
-- value: string name from above or array item
theming.titlebar = "Solo"

-- Set titlebar font
theming.titlebar_font = "manrope bold 10"


--=======================================================
--    🚥 STATUS BAR
------------------------
-- Do you like status bar?
-- value: [true | false]
theming.enable_statusbar = true

-- Randomize theme on every restart
-- value: [true | false]
theming.random_statusbar = false

-- List of available theme names
theming.statusbar_themes = {
	"Solo"		-- 1 : Default
}

-- Set status bar theme
-- value: string name from above or array item
theming.statusbar = "Solo"

-- How much bloated do you want?
-- value:
-- 	"compact"	-- only one bar
-- 	"extended"	-- add a extra bar for main screen
theming.statusbar_mode = "compact"

-- Set status bar font
theming.statusbar_font = "manrope medium 10"


--=======================================================
--    🧰 DOCKS
------------------------
-- Would you like a dock bar?
-- value: [true | false]
theming.enable_dock = false

-- Randomize theme on every restart
-- value: [true | false]
theming.random_dock = false

-- List of available theme names
theming.dock_themes = {
	"Bottom"	-- 1 : Default
}

-- Set status bar theme
-- value: string name from above or array item
theming.dock = "Bottom"

-- How much bloated do you want?
-- value:
-- 	"compact"	-- only apps
-- 	"extended"	-- add folder and others
theming.dock_mode = "compact"

-- On which screen should the dock be displayed?
-- value: "all", 1, 2, 3, .. [int]
theming.dock_screen = 1


return theming
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
