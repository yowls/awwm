local settings = {}

--    NOTIFICATIONS
-----------------------
-- set border to show notifications
-- string values:
-- 		top_right
-- 		top_left
-- 		bottom_left
-- 		bottom_right
-- 		top_middle
-- 		bottom_middle
-- default: "top_right"
settings.notification_position = "bottom_left"


--    LAYOUTS
------------------------
-- Comment or Uncomment for Disable/Enable layout
-- available on all monitors
settings.layouts_list = {
	"max",
	-- "max_fullscreen",
	"magnifier",
	"tile",
	-- "tile_left",
	"tile_bottom",
	-- "tile_top",
	-- "fair",
	-- "fair_horizontal",
	-- "spiral",
	-- "spiral_dwindle",
	-- "corner_nw",
	-- "corner_ne",
	-- "corner_sw",
	-- "corner_se",
	"floating"
}


--    TAGLIST
------------------------
-- Labels on tags
-- determines the number and the label of tag per monitor
-- example: {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"}	-- 10 tags
-- example: {"α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ"}
-- example: {"하나", "두", "세", "네", "다섯", "여섯", "일곱", "여덟", "아홉", "십"}
-- example: {"", "", "", "", "ﭮ", "", "", "", "", ""}
settings.tag_label_screens = {
	{"α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ"}, -- screen 1 : 10 tags
	{"α", "β", "γ", "δ", "ε", "ζ"}							-- screen 2 : 5 tags
}

-- Layouts on Tags
-- define what layout use in what tag in what monitor
-- see all available names in layouts_list
settings.tag_layout_screens = {
	{	-- screen 1
		"tile",				-- 1
		"max",				-- 2
		"tile_bottom",		-- 3
		"tile",				-- 4
		"max",				-- 5
		"tile_bottom",		-- 6
		"tile_bottom",		-- 7
		"tile_bottom",		-- 8
		"tile",				-- 9
		"floating"			-- 10
	},
	{	-- screen 2
		"tile",				-- 1
		"max",				-- 2
		"tile_bottom",		-- 3
		"tile",				-- 4
		"max"				-- 5
	}
}

return settings
