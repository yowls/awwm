--[[
.   ______     __     __     ______     ______     ______     __    __     ______
.  /\  __ \   /\ \  _ \ \   /\  ___\   /\  ___\   /\  __ \   /\ "-./  \   /\  ___\
.  \ \  __ \  \ \ \/ ".\ \  \ \  __\   \ \___  \  \ \ \/\ \  \ \ \-./\ \  \ \  __\
.   \ \_\ \_\  \ \__/".~\_\  \ \_____\  \/\_____\  \ \_____\  \ \_\ \ \_\  \ \_____\
.    \/_/\/_/   \/_/   \/_/   \/_____/   \/_____/   \/_____/   \/_/  \/_/   \/_____/
.      gitlab.com/yowls/awesomewm-rice
--]]

--------------------
-- IMPORT MODULES --
--------------------

-- Lua libraries
pcall(require, "luarocks.loader")

-- Awesome libraries
local beautiful	= require("beautiful")

-- Import custom config
local debug	= require("library.helpers.debug")
local paths	= require("settings.paths")


---------------
-- PRE-START --
---------------

-- Set initial time mark
local starting = os.time()

-- Debug errors
debug.notify_errors()

-- Log directory
debug.check_log_dir()


--------------------
-- LOAD BEAUTIFUL --
--------------------
beautiful.init( paths.awesome_themes .. "init.lua" )


----------------------
-- TAGS AND LAYOUTS --
----------------------
require("library.tag_layout")


-----------
-- RULES --
-----------
require("library.rules")
require("library.notification")


--------------
-- BINDINGS --
--------------
require("library.keybinds")


-----------------
-- LOAD THEMES --
-----------------
require("themes.notifications")
require("themes.titlebar")
require("themes.statusbar")
require("themes.docks")


-----------------------
-- AUTOSTART MODULES --
-----------------------
require("library.autostart")


------------
-- EXTRAS --
------------
-- require("extras").init()


------------
-- ENDING --
------------

-- Set final mark
local ending = os.time()
local start_delay = ending-starting

debug.log(string.format("End game in: %s ms", start_delay))


-- vim: filetype=lua tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
